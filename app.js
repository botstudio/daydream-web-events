const express = require("express")
const app = express()

const { randomBytes } = require("crypto")

const port = process.env.PORT || 5050

const dotenv = require("dotenv")

dotenv.config()

app.use(require("cors")())

app.use(express.json())

let events = []
const correct = process.env.DDWE_PASSWORD

app.post("/event/push", (req, res) => {
    if (!req.body.eventname || !req.body.pass || !req.body.content) return res.status(400).send({
        error: "This needs more body parameters."
    })
    if (req.body.pass !== correct) return res.status(403).send({
        error: "I need the CORRECT password."
    })
    events.push({
        name: req.body.eventname,
        content: req.body.content,
        id: randomBytes(32).toString("hex")
    })
    res.send({ success: true })
})

app.get("/event/all", (req, res) => {
    if (!req.header("Authorization")) return res.status(400).send({
        error: "I need a password."
    })
    if (req.header("Authorization") !== correct) return res.status(403).send({
        error: "I need the CORRECT password."
    })
    res.json(events)
})

app.use((req, res) => {
    res.status(404).json({
        error: "You crash-landed on PNF-404."
    })
})

app.listen(port, () => {
    console.log(`Listening! http://localhost:${port}`)
})